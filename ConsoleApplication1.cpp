﻿#include <iostream>

void print(bool param, int N)
{
    for (int i = param; i <= N; i+=2)
    {
        std::cout << i;
    }    
}

int main()
{
    int N = 22;

    for (int i = 0; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i;
        }
    }

    std::cout << std::endl;

    print(false, N);
}